class HelloController < Crails::BaseController

  def index
    name = @params.has_key?("name") ? @params["name"] : "nobody"
    "
      <html>
        <body>
          Hello there, #{name}!
        </body
      </html>
    "
  end

end

routes do
  get "/hello", { to: "hello#index" }

  root "welcome#index"
end
